package com.klymchuk.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BusinessLogic implements Model {
    @Override
    public String printAnnotateFields() {
        StringBuilder stringBuilder = new StringBuilder();

        Class annotationClass = MyAnnotationClass.class;

        for (Field f : annotationClass.getDeclaredFields()) {
            if (f.isAnnotationPresent(MyAnnotation.class)) {
                stringBuilder.append("\nname: ").append(f.getAnnotation(MyAnnotation.class).name())
                        .append("  age: ").append(f.getAnnotation(MyAnnotation.class).age());
                stringBuilder.append("\n").append(f.getAnnotation(MyAnnotation.class));
            }
        }

        return stringBuilder.toString();
    }

    @Override
    public String invokeMethods() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        StringBuilder stringBuilder = new StringBuilder();

        MyAnnotationClass myAnnotationClass = new MyAnnotationClass();

        Class clazz = myAnnotationClass.getClass();
        Method method = clazz.getDeclaredMethod("method");
        method.setAccessible(true);
        stringBuilder.append("\n").append(method.invoke(myAnnotationClass));

        method = clazz.getDeclaredMethod("method", String.class, int[].class);
        method.setAccessible(true);
        int[] arrInt = new int[]{1, 2, 3, 4};
        stringBuilder.append("\n").append(method.invoke(myAnnotationClass, "my method: ", arrInt));

        method = clazz.getDeclaredMethod("method", String.class, String[].class);
        method.setAccessible(true);
        String[] arrString = new String[]{"one", "two", "three", "four"};
        stringBuilder.append("\n").append(method.invoke(myAnnotationClass, "my method", arrString));

        return stringBuilder.toString();
    }

    @Override
    public String setValueToUnknowingField() throws NoSuchFieldException, IllegalAccessException {

        MyAnnotationClass myAnnotationClass = new MyAnnotationClass();

        Class clazz = myAnnotationClass.getClass();

        Field field = clazz.getDeclaredField("valueWithoutAnnotation");

        field.set(myAnnotationClass, 23);

        return field.getName() + "  " + field.get(myAnnotationClass);
    }

    @Override
    public <T> String getInformation(T obj) {
        MyAnnotationClass myAnnotationClass = new MyAnnotationClass();

        return myAnnotationClass.getInformation(obj);
    }
}
