package com.klymchuk.model;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MyAnnotationClass {
    @MyAnnotation(age = 23)
    public String name;
    @MyAnnotation(name = "Oleg", age = 12)
    public int value = 45;
    public int valueWithoutAnnotation = 56;

    private String method() {
        return "Empty method";
    }

    private String method(String text, int... args) {
        return text + Arrays.toString(args);
    }

    private String method(String text, String... args) {
        return text + Arrays.toString(args);
    }

    public  <T> String getInformation(T object) {
        return object.getClass().toString() + " " + object.toString();
    }
}
