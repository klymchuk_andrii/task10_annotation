package com.klymchuk.model;

import java.lang.reflect.InvocationTargetException;

public interface Model {
    String printAnnotateFields();
    String invokeMethods() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;
    String setValueToUnknowingField() throws NoSuchFieldException, IllegalAccessException;
    <T> String getInformation(T obj);
}
