package com.klymchuk.controller;

import com.klymchuk.model.BusinessLogic;
import com.klymchuk.model.Model;

import java.lang.reflect.InvocationTargetException;

public class ControllerImp implements Controller {
    private Model model;

    public ControllerImp(){
        model = new BusinessLogic();
    }

    @Override
    public String printAnnotateFields() {
        return model.printAnnotateFields();
    }

    @Override
    public String invokeMethods() {
        String answer = "";
        try{
            answer = model.invokeMethods();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return answer;
    }

    @Override
    public String setValueToUnknowingField() {
        try {
            return model.setValueToUnknowingField();
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String getInformation() {
        return model.getInformation(new String("Value"));
    }
}
