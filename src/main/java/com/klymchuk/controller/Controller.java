package com.klymchuk.controller;

public interface Controller {
    String printAnnotateFields();
    String invokeMethods();
    String setValueToUnknowingField();
    String getInformation();
}
